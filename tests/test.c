#include<check.h>

#include "libwarc.h"

START_TEST(test_double)
{
    int x;
    x = print_hw(5);
    ck_assert_int_eq(x, 10);
} END_TEST

Suite *libwarc_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("libwarc");
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_double);
    suite_add_tcase(s, tc_core);

    return s;
}

int main()
{
    int no_failed = 0;
    Suite *s;
    SRunner *runner;

    s = libwarc_suite();
    runner = srunner_create(s);

    srunner_set_tap(runner, "libwarc.tap");
    srunner_run_all(runner, CK_NORMAL);
    no_failed = srunner_ntests_failed(runner);
    srunner_free(runner);
    return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}